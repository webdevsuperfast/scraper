var cloudscaper = require('cloudscraper'),
    fs = require('fs'),
    each = require('foreach');

// For Single Page
// cloudscaper.get('http://jetbet.gaminggap.com/About/', function(error, response, body) {
//     if (error) {
//         console.log('Error occured');
//     } else {
//         // console.log(body);
//         fs.writeFile('about.html', body, function(err) {
//             if (err) console.log(err);
//         });
//     }
// });

// For Multiple Pages
each({'index.html':'http://jetbet.gaminggap.com/','about.html':'http://jetbet.gaminggap.com/About',
'responsible_gambling.html':'http://jetbet.gaminggap.com/Responsible-Gambling','privacy_policy.html':'http://jetbet.gaminggap.com/Privacy-Policy'}, function(value, key){
    cloudscaper.get(value, function(error, response, body) {
        if (error) {
            console.log('Error occured');
        } else {
            fs.writeFile(key, body, function(err) {
                if (err) console.log(err);
            });
        }
    });
});